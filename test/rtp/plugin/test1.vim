execute frawor#Setup('0.0', {'@%oop': '1.0'})
let s:foo={}
function s:foo.echo()
    echom 'Echoing' self.__class__.name
    echom string(sort(keys(self)))
    echom string(self.__variables__)
    echom string(filter(copy(self), 'v:key[0] isnot# "_" && type(v:val)!=2'))
    echom '^^^^^^^^^^^^^'
endfunction
let s:foobar={}
function s:foobar.echo()
    echom 'Echoing' self.__class__.name '(2)'
endfunction
let s:_classes.Foo={}
call s:_f.pyoop.class('Foo', s:foo, {'foovar': 1})
call s:_f.pyoop.class('Bar', {},    {},            ['Foo'])
call s:_f.pyoop.class('Baz', {},    {'foovar': 2}, ['Bar'])
call s:_f.pyoop.class('FooBar', s:foobar, {})
call s:_f.pyoop.class('FFB', {}, {}, ['Foo', 'FooBar'])
call s:_f.pyoop.class('FBF', {}, {}, ['FooBar', 'Foo'])
call s:_f.pyoop.class('Abc', s:foobar, {}, ['Foo'])
call s:_f.pyoop.class('Def', {}, {}, ['Abc'])
let foo=s:_classes.Foo.new()
let bar=s:_classes.Bar.new()
let baz=s:_classes.Baz.new()
let ffb=s:_classes.FFB.new()
let fbf=s:_classes.FBF.new()
let abc=s:_classes.Abc.new()
let def=s:_classes.Def.new()
call foo.echo()
call bar.echo()
call baz.echo()
call ffb.echo()
call fbf.echo()
call abc.echo()
call def.echo()
echom '--------------'
echom ': foo.foovar=3'
let foo.foovar=3
echom ' foo:'
call foo.echo()
echom ' New Foo:'
call s:_classes.Foo.new().echo()
echom 'Instanceof:'
echom ' foo    of Bar:' s:_r.pyoop.instanceof('Bar', foo)
echom ' bar    of Bar:' s:_r.pyoop.instanceof('Bar', bar)
echom ' baz    of Bar:' s:_r.pyoop.instanceof('Bar', baz)
echom ' baz    of Foo:' s:_r.pyoop.instanceof('Foo', baz)
echom ' ffb    of Foo:' s:_r.pyoop.instanceof('Foo', ffb)
echom ' def    of Foo:' s:_r.pyoop.instanceof('Foo', def)
echom ' foobar of Foo:' s:_r.pyoop.instanceof('Foo', s:_classes.FooBar.new())
